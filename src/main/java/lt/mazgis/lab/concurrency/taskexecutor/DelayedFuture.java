package lt.mazgis.lab.concurrency.taskexecutor;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Delayed;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public interface DelayedFuture<T> extends Delayed,Future<T> {
}
