package lt.mazgis.lab.concurrency.taskexecutor;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

public class ExecutorMetrics {
	private final MetricRegistry metric;
	private final Timer timer;
	private final Counter counter;
	private final Counter pending;
	private final Counter errors;
	private final Counter canceled;

	public ExecutorMetrics(String name) {
		metric = new MetricRegistry();
		timer = metric.timer(MetricRegistry.name(name, "execution", "time"));
		counter = metric.counter(MetricRegistry.name(name, "execution", "active", "threads"));
		pending = metric.counter(MetricRegistry.name(name, "execution", "pending", "threads"));
		errors = metric.counter(MetricRegistry.name(name, "execution", "error"));
		canceled = metric.counter(MetricRegistry.name(name, "execution", "canceled", "threads"));
	}
	
	public Timer.Context onExecute() {
		counter.inc();
		pending.dec();
		return timer.time();
	}
	
	public void onExecutionDone(Timer.Context timer) {
		timer.stop();
		counter.dec();
	}
	
	public void onError() {
		errors.inc();
	}
	
	public void onAdd() {
		pending.inc();
	}
	
	public void onCanel() {
		canceled.inc();
		pending.dec();
	}

	public MetricRegistry getMetric() {
		return metric;
	}

	public Timer getTimer() {
		return timer;
	}

	public Counter getCounter() {
		return counter;
	}

	public Counter getPending() {
		return pending;
	}

	public Counter getErrors() {
		return errors;
	}

	public Counter getCanceled() {
		return canceled;
	}
	


}
