package lt.mazgis.lab.concurrency.taskexecutor;

import java.util.concurrent.Callable;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.Timer;

public class Executor {

	private final ForkJoinPool executorService;
	private final DelayQueue<DelayedFuture<?>> delayQueue;
	private final ExecutorMetrics metric;

	public Executor(int size, DelayQueue<DelayedFuture<?>> delayQueue, ExecutorMetrics metrics) {
		executorService = new ForkJoinPool(size + 1, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
		this.delayQueue = delayQueue;
		this.metric = metrics;
		executorService.execute(() -> removeDelayedTask());
	}

	public Executor(int size) {
		executorService = new ForkJoinPool(size + 1, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
		delayQueue = new DelayQueue<>();
		metric = new  ExecutorMetrics(getClass().getName());
		executorService.execute(() -> removeDelayedTask());
	}

	private class MonitoredTask<T> extends ForkJoinTask<T> implements DelayedFuture<T> {
		private static final long serialVersionUID = -1678595210359379953L;
		final long origin;
		final long delay;
		private final Callable<T> callable;
		private T result;

		private MonitoredTask(Callable<T> callable, long delay, TimeUnit timeUnit) {
			this.origin = System.currentTimeMillis();
			this.delay = timeUnit.convert(delay, TimeUnit.MILLISECONDS);
			this.callable = callable;
		}

		@Override
		public int compareTo(Delayed o) {
			long diff = (getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS));
			return ((diff == 0) ? 0 : ((diff < 0) ? -1 : 1));
		}

		@Override
		public long getDelay(TimeUnit unit) {
			return unit.convert(delay - (System.currentTimeMillis() - origin), TimeUnit.MILLISECONDS);
		}

		@Override
		public boolean exec() {
			Timer.Context timer = metric.onExecute();
			try {
				delayQueue.remove(this);
				result = callable.call();
				return true;
			} catch (Error | RuntimeException err) {
				metric.onError();
				throw err;
			} catch (Exception ex) {
				metric.onError();
				throw new RuntimeException(ex);
			} finally {
				metric.onExecutionDone(timer);
			}
		}

		@Override
		public T getRawResult() {
			return result;
		}

		@Override
		protected void setRawResult(T value) {
			result = value;
		}
	}
	
	public <T> Future<T> executeTask(Callable<T> c, long timout, TimeUnit timeUnit) {
		metric.onAdd();
		MonitoredTask<T> task = new MonitoredTask<>(c, timout, timeUnit);
		executorService.submit(task);
		delayQueue.put(task);
		return task;
	}

	private void removeDelayedTask() {
		boolean interrupted = false;
		while (!interrupted) {
			try {
				Future<?> f = delayQueue.take();
				if (f.cancel(false)) {
					metric.onCanel();
				}
			} catch (InterruptedException e) {
				interrupted = true;
			}
		}
	}
}
