package lt.mazgis.lab.concurrency.taskexecutor.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import lt.mazgis.lab.concurrency.taskexecutor.DelayedFuture;
import lt.mazgis.lab.concurrency.taskexecutor.Executor;
import lt.mazgis.lab.concurrency.taskexecutor.ExecutorMetrics;

public class TaksExecutorTest {

	@Test
	public void testTaskCancelation() {
		DelayQueue<DelayedFuture<?>> delayQueue = new DelayQueue<>();
		ExecutorMetrics metrics = new ExecutorMetrics(Executor.class.getName());
		Executor executor = new Executor(1, delayQueue, metrics);
		Future<String> longTaskFirst = executor.executeTask(createCallable(100), 1000, TimeUnit.MILLISECONDS);
		Future<String> shortCanceledTaskSecond = executor.executeTask(createCallable(10), 10, TimeUnit.MILLISECONDS);

		assertTrue(delayQueue.size() == 2);

		CompletableFuture<String> shortTaskFuture = CompletableFuture.supplyAsync(() -> {
			try {
				return shortCanceledTaskSecond.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException(e);
			}
		}).handle((r, t) -> {
			assertNull(r);
			assertNotNull(t);
			assertTrue(t.getCause() instanceof CancellationException);
			assertEquals(0, delayQueue.size());
			assertEquals(1, metrics.getCounter().getCount());
			assertEquals(1, metrics.getCanceled().getCount());
			return null;
		});

		CompletableFuture<String> longTaskFuture = CompletableFuture.supplyAsync(() -> {
			try {
				return longTaskFirst.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException(e);
			}
		}).whenComplete((r, t) -> {
			assertNotNull(r);
			assertTrue(r.equals("OK"));
			assertNull(t);
		});

		longTaskFuture.join();
		shortTaskFuture.join();
		
		assertEquals(0,delayQueue.size());
		assertEquals(true,shortCanceledTaskSecond.isCancelled());
		assertEquals(false,longTaskFirst.isCancelled());

		assertEquals(0, metrics.getCounter().getCount());
		assertEquals(1, metrics.getCanceled().getCount());
		assertEquals(0, metrics.getErrors().getCount());
		assertEquals(1, metrics.getTimer().getCount());
		assertEquals(0, metrics.getPending().getCount());
	}

	private Callable<String> createCallable(long delay) {
		return () -> {
			Thread.sleep(delay);
			return "OK";
		};
	}

	private void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
	}
}
